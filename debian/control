Source: templayer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ian Ward <ian@excess.org>
Build-Depends: dh-python, debhelper (>= 5.0.37.2), cdbs (>= 0.4.42), python
Standards-Version: 3.8.1
Homepage: http://excess.org/templayer/
Vcs-Git: https://salsa.debian.org/python-team/packages/templayer.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/templayer

Package: python-templayer
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}
Provides: ${python:Provides}
Description: layered template library for Python
 Templayer currently supports only HTML generation, but its simple design
 is easily extended to support other file formats.
 .
 Templayer was created to offer an alternative to the more common ways
 of generating dynamic HTML: embedding code within the HTML (PHP etc.),
 or embedding HTML within code (traditional CGI). Neither of these
 methods allow for a clean separation of the form, or layout, of a page
 and the function of page generation. Instead of mixing HTML and Python,
 two rich and extremely expressive languages, we can add a small amount
 of syntax to each and keep the two separate and coherent.
